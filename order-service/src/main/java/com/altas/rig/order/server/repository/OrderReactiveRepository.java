package com.altas.rig.order.server.repository;

import com.altas.rig.order.server.model.entity.Order;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderReactiveRepository extends ReactiveMongoRepository<Order, String> {

}
