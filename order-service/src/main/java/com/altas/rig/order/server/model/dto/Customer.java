package com.altas.rig.order.server.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.*;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({
        "id",
        "firstName",
        "lastName",
        "phoneNumber",
        "email",
        "address",
        "creditLimit",
        "version",
        "createdBy",
        "createdDate",
        "updatedBy",
        "updatedDate"
})
public class Customer implements Serializable {
    private static final long serialVersionUID = 6938865787322676118L;

    @Id
    @JsonProperty("id")
    public String id;
    @JsonProperty("firstName")
    public String firstName;
    @JsonProperty("lastName")
    public String lastName;
    @JsonProperty("phoneNumber")
    public long phoneNumber;
    @JsonProperty("email")
    public String email;
    @JsonProperty("address")
    public Address address;
    @JsonProperty("creditLimit")
    public Double creditLimit;
    @Version
    @JsonProperty("version")
    public long version;
    @CreatedBy
    @JsonProperty("createdBy")
    public String createdBy;
    @CreatedDate
    @JsonProperty("createdDate")
    public Date createdDate;
    @LastModifiedBy
    @JsonProperty("updatedBy")
    public String updatedBy;
    @LastModifiedDate
    @JsonProperty("updatedDate")
    public Date updatedDate;
}
