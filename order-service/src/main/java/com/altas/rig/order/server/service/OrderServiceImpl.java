package com.altas.rig.order.server.service;

import com.altas.rig.order.server.enums.ActionType;
import com.altas.rig.order.server.exception.InternalServerError;
import com.altas.rig.order.server.model.dto.CustomerCreditLimitEventDTO;
import com.altas.rig.order.server.model.dto.ProductCountEventDTO;
import com.altas.rig.order.server.model.entity.Order;
import com.altas.rig.order.server.producer.ProductCustomerMsgProducer;
import com.altas.rig.order.server.repository.OrderReactiveRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    ReactiveMongoTemplate reactiveMongoTemplate;
    @Autowired
    ProductCustomerMsgProducer productCustomerMsgProducer;
    @Autowired
    ProductCountEventDTO productCountEventDTO;
    @Autowired
    CustomerCreditLimitEventDTO customerCreditLimitEventDTO;
    @Autowired
    private OrderReactiveRepository orderReactiveRepository;

    @Override
    public Mono<Order> createOrder(Order order) {
        Mono<Order> insOrder = orderReactiveRepository.insert(order);
        insOrder.subscribe(this::initiateKafkaEventForCreate);
        return insOrder;
    }

    @Override
    public Mono<Order> getOrderById(String orderId) {
        return orderReactiveRepository.findById(orderId);
    }

    @Override
    public Flux<Order> getAllOrder() {
        return orderReactiveRepository.findAll();
    }

    @Override
    public Mono<Order> updateOrder(String orderId, Order updatedOrder) {

        Mono<Order> returnUpdatedOrder = orderReactiveRepository.save(updatedOrder);
        Mono<Order> order = getOrderById(orderId);

        order.subscribe(oldOrder -> initiateKafkaEventForUpdate(oldOrder, updatedOrder, orderId));

        return returnUpdatedOrder;
    }

    @Override
    public Mono<Order> deleteOrder(String orderId) {

        final Mono<Order> order = getOrderById(orderId);
        if (Objects.isNull(order)) {
            return Mono.empty();
        }
        Mono<Order> returnDeletedOrder = getOrderById(orderId).switchIfEmpty(Mono.empty()).filter(Objects::nonNull)
                .flatMap(orderToBeDeleted -> orderReactiveRepository.delete(orderToBeDeleted)
                        .then(Mono.just(orderToBeDeleted)));

        order.subscribe(this::initiateKafkaEventForDelete);
        return returnDeletedOrder;

    }

    /****************** Start :: intiate kafka event for create ******************/
    private void initiateKafkaEventForCreate(Order order) {

        productCountEventDTO.setOrderId(order.getOrderId());
        productCountEventDTO.setProductId(order.getProduct().getProductId());
        productCountEventDTO.setProductCount(order.getProduct().getProductCount());
        productCountEventDTO.setActionType(ActionType.COUNT_DECREMENT);

        customerCreditLimitEventDTO.setOrderId(order.getOrderId());
        customerCreditLimitEventDTO.setCustomerId(order.getCustomerId());
        customerCreditLimitEventDTO.setCreditLimitAmount(order.getOrderAmount());
        customerCreditLimitEventDTO.setActionType(ActionType.CREDIT_DECREMENT);

        try {
            productCustomerMsgProducer.sendToProduct(productCountEventDTO);
            productCustomerMsgProducer.sendToCustomer(customerCreditLimitEventDTO);
        } catch (Exception e) {
            throwRuntimeException(e);
        }
    }

    /****************** End :: intiate kafka event for create ******************/

    /****************** Start :: intiate kafka event for update order ******************/
    private void initiateKafkaEventForUpdate(Order oldOrder, Order updatedOrder, String orderId) {

        /****************** Start :: Product Count Calculation ******************/
        if (oldOrder.getProduct().getProductCount() != updatedOrder.getProduct().getProductCount()) {

            productCountEventDTO.setOrderId(orderId);
            productCountEventDTO.setProductId(updatedOrder.getProduct().getProductId());

            if (oldOrder.getProduct().getProductCount() > updatedOrder.getProduct().getProductCount()) {
                int countDiff = (oldOrder.getProduct().getProductCount())
                        - (updatedOrder.getProduct().getProductCount());
                productCountEventDTO.setProductCount(countDiff);
                productCountEventDTO.setActionType(ActionType.COUNT_INCREMENT);

            } else {
                int countDiff = (updatedOrder.getProduct().getProductCount())
                        - (oldOrder.getProduct().getProductCount());
                productCountEventDTO.setProductCount(countDiff);
                productCountEventDTO.setActionType(ActionType.COUNT_DECREMENT);
            }
            try {
                productCustomerMsgProducer.sendToProduct(productCountEventDTO);
            } catch (Exception e) {
                throwRuntimeException(e);
            }
        }

        /****************** End :: Product Count Calculation ******************/

        /****************** Start :: Customer Credit Limit Calculation ******************/

        if (oldOrder.getOrderAmount() != updatedOrder.getOrderAmount()) {

            customerCreditLimitEventDTO.setOrderId(orderId);
            customerCreditLimitEventDTO.setCustomerId(updatedOrder.getCustomerId());

            if (oldOrder.getOrderAmount() > updatedOrder.getOrderAmount()) {
                double creditDiff = (oldOrder.getOrderAmount()) - (updatedOrder.getOrderAmount());
                customerCreditLimitEventDTO.setCreditLimitAmount(creditDiff);
                customerCreditLimitEventDTO.setActionType(ActionType.CREDIT_INCREMENT);
            } else {
                double creditDiff = (updatedOrder.getOrderAmount()) - (oldOrder.getOrderAmount());
                customerCreditLimitEventDTO.setCreditLimitAmount(creditDiff);
                customerCreditLimitEventDTO.setActionType(ActionType.CREDIT_DECREMENT);

            }
            try {

                productCustomerMsgProducer.sendToCustomer(customerCreditLimitEventDTO);
            } catch (Exception e) {
                throwRuntimeException(e);
            }

        }

        /****************** End :: Credit Limit Calculation ******************/

    }
    /****************** End :: intiate kafka event for update order ******************/

    /****************** Start :: intiate kafka event for delete ******************/
    private void initiateKafkaEventForDelete(Order order) {

        productCountEventDTO.setOrderId(order.getOrderId());
        productCountEventDTO.setProductId(order.getProduct().getProductId());
        productCountEventDTO.setProductCount(order.getProduct().getProductCount());
        productCountEventDTO.setActionType(ActionType.COUNT_INCREMENT);

        customerCreditLimitEventDTO.setOrderId(order.getOrderId());
        customerCreditLimitEventDTO.setCustomerId(order.getCustomerId());
        customerCreditLimitEventDTO.setCreditLimitAmount(order.getOrderAmount());
        customerCreditLimitEventDTO.setActionType(ActionType.CREDIT_INCREMENT);

        try {
            productCustomerMsgProducer.sendToProduct(productCountEventDTO);
            productCustomerMsgProducer.sendToCustomer(customerCreditLimitEventDTO);
        } catch (Exception e) {
            throwRuntimeException(e);
        }

    }

    /****************** End :: intiate kafka event for delete ******************/


    private void throwRuntimeException(Exception e) {
        throw new InternalServerError(e.toString());
    }
}
