package com.altas.rig.order.server.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "Product")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@JsonPropertyOrder({
        "producId",
        "productName",
        "productDesc",
        "qtyInStock",
        "prodCategory",
        "price",
        "prodManufacturer",
        "prodMfgDate",
        "version",
        "vendor",
        "createdBy",
        "createdDate",
        "updatedBy",
        "updatedDate"
})
public class Product implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @JsonProperty("producId")
    private String producId;
    @JsonProperty("productName")
    private String productName;
    @JsonProperty("productDesc")
    private String productDesc;
    @JsonProperty("qtyInStock")
    private Integer qtyInStock;
    @JsonProperty("prodCategory")
    private String prodCategory;
    @JsonProperty("price")
    private String price;
    @JsonProperty("prodManufacturer")
    private String prodManufacturer;
    @JsonProperty("prodMfgDate")
    private String prodMfgDate;
    @JsonProperty("vendor")
    private String vendor;
    @JsonProperty("version")
    private String version;
    @JsonProperty("createdBy")
    private String createdBy;
    @JsonProperty("createdDate")
    private String createdDate;
    @JsonProperty("updatedBy")
    private String updatedBy;
    @JsonProperty("updatedDate")
    private String updatedDate;
}
