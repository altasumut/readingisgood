package com.altas.rig.order.server.model.dto;

import com.altas.rig.order.server.enums.ActionType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductCountEventDTO {
    protected String orderId;
    protected String productId;
    protected int productCount;
    protected ActionType actionType;
}
