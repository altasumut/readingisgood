package com.altas.rig.order.server.model.dto;

import com.altas.rig.order.server.enums.ActionType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerCreditLimitEventDTO {
    protected String orderId;
    protected String customerId;
    protected double creditLimitAmount;
    protected ActionType actionType;
}
