package com.altas.rig.order.server.service;

import com.altas.rig.order.server.model.entity.Order;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface OrderService {

	Mono<Order> createOrder(Order order);

	Mono<Order> getOrderById(String orderId);

	Flux<Order> getAllOrder();

	Mono<Order> updateOrder(String orderId, Order order);

	Mono<Order> deleteOrder(String orderId);

}
