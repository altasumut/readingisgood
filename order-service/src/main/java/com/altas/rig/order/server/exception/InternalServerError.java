package com.altas.rig.order.server.exception;

public class InternalServerError extends RuntimeException {
    public InternalServerError(String errorMessage) {
        super(errorMessage);
    }
}