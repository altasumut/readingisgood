package com.altas.rig.order.server.util;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;

public class TokenUtil {

    private static final String TOKEN_TYPE = "Bearer";

    private TokenUtil() {
        throw new IllegalStateException("TokenUtil class");
    }

    public static String getToken() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (authentication != null && authentication.getDetails() instanceof OAuth2AuthenticationDetails) {
            OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails) authentication.getDetails();
            return String.format("%s %s", TOKEN_TYPE, details.getTokenValue());
        } else return null;
    }
}
