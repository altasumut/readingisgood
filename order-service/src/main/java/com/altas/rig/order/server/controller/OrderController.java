package com.altas.rig.order.server.controller;

import com.altas.rig.order.server.model.dto.Customer;
import com.altas.rig.order.server.model.dto.Product;
import com.altas.rig.order.server.model.entity.Order;
import com.altas.rig.order.server.service.OrderService;
import com.altas.rig.order.server.util.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequestMapping("/api/order")
public class OrderController {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Autowired
    OrderService orderService;

    @PostMapping(value = "/createOrder")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<ResponseEntity<?>> createOrder(Authentication authentication, @RequestBody Order order) {

        Mono<Customer> customerResponse = WebClient.create().get()
                .uri("http://localhost:8081/api/customer/{id}", order.getCustomerId())
                .header(AUTHORIZATION_HEADER, TokenUtil.getToken())
                .retrieve()
                .bodyToMono(Customer.class);

        Mono<Product> productResponse = WebClient.create().get()
                .uri("http://localhost:8083/api/product/getProduct/{id}", order.getProduct().getProductId())
                .header(AUTHORIZATION_HEADER, TokenUtil.getToken())
                .retrieve()
                .bodyToMono(Product.class);

        return Mono.zip(customerResponse, productResponse)
                .map(response -> {
                    Customer customer = response.getT1();
                    Product product = response.getT2();

                    Integer productQtyInStock = product.getQtyInStock();
                    if (productQtyInStock < order.getProduct().getProductCount()) {
                        return new ResponseEntity<>("Due to limited stock, Maximum Order limit for this product is " + productQtyInStock,
                                HttpStatus.BAD_REQUEST);
                    }

                    double currentCreditLimit = customer.getCreditLimit();
                    order.setOrderAmount(Double.parseDouble(product.getPrice()));
                    if (currentCreditLimit < order.getOrderAmount()) {
                        return new ResponseEntity<>("Customer does not have sufficient Credit limit to place the Order.",
                                HttpStatus.BAD_REQUEST);
                    }

                    order.setOrderId(UUID.randomUUID().toString());
                    orderService.createOrder(order);
                    return new ResponseEntity<>(order, HttpStatus.OK);
                });
    }

    @GetMapping(value = "/getAllOrder")
    public Flux<Order> getAllOrder() {

        return orderService.getAllOrder();

    }

    @GetMapping(value = "/getOrder/{id}")
    public Mono<ResponseEntity<Order>> getOrderById(@PathVariable String id) {

        return orderService.getOrderById(id).map(order -> new ResponseEntity<>(order, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping(value = "/updateOrder/{id}")
    public Mono<ResponseEntity<?>> updateOrder(@PathVariable String id, @RequestBody Order order) {

        Mono<Customer> customerResponse = WebClient.create().get()
                .uri("http://localhost:8093/customer-microservice/customers/{id}", order.getCustomerId())
                .retrieve()
                .bodyToMono(Customer.class);

        Mono<Product> productResponse = WebClient.create().get()
                .uri("http://localhost:8093/product-microservice/product/getProduct/{id}", order.getProduct().getProductId())
                .retrieve()
                .bodyToMono(Product.class);

        return Mono.zip(customerResponse, productResponse)
                .map(response -> {
                    Customer customer = response.getT1();
                    Product product = response.getT2();

                    Integer productQtyInStock = product.getQtyInStock();
                    if (productQtyInStock < order.getProduct().getProductCount()) {
                        return new ResponseEntity<>("Due to limited stock, Maximum Order limit for this product is " + productQtyInStock,
                                HttpStatus.BAD_REQUEST);
                    }

                    double currentCreditLimit = customer.getCreditLimit();
                    if (currentCreditLimit < order.getOrderAmount()) {
                        return new ResponseEntity<>("Customer does not have sufficient Credit limit to place the Order.",
                                HttpStatus.BAD_REQUEST);
                    }

                    order.setOrderId(id);
                    orderService.updateOrder(id, order);
                    return new ResponseEntity<>(order, HttpStatus.OK);
                });
    }

    @DeleteMapping(value = "/deleteOrder/{id}")
    public Mono<ResponseEntity<Order>> deleteOrder(@PathVariable String id) {

        return orderService.deleteOrder(id).map(order -> new ResponseEntity<>(order, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}
