package com.altas.rig.order.server.constant;

public class Constants {

    public static final String ORDER_ID_KEY = "id";
    public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss.SSS";

    private Constants() {
        throw new IllegalStateException("Constants class");
    }

}
