package com.altas.rig.order.server.enums;

public enum ActionType {

    CREDIT_INCREMENT,
    CREDIT_DECREMENT,
    COUNT_INCREMENT,
    COUNT_DECREMENT

}
