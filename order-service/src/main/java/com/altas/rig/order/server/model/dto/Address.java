
package com.altas.rig.order.server.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonPropertyOrder({
        "addressLine1",
        "addressLine2",
        "city",
        "state",
        "zipCode",
        "country"
})
public class Address implements Serializable {
    private static final long serialVersionUID = -3930009017282385831L;

    @JsonProperty("addressLine1")
    public String addressLine1;
    @JsonProperty("addressLine2")
    public String addressLine2;
    @JsonProperty("city")
    public String city;
    @JsonProperty("state")
    public String state;
    @JsonProperty("zipCode")
    public long zipCode;
    @JsonProperty("country")
    public String country;
}
