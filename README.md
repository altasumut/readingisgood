# ReadingIsGood Microservice Architecture - Saga Choreography

#### Spring Boot, Spring Security, OAuth2, Redis, MongoDB, Kafka, Webflux


### Hakkında

İlgili proje için Mikroservis mimarisi hedeflenmiş olup Saga Choreography pattern uygulanmıştır.
Yetkilendirme protokolü olarak Oauth2 kullanıldı. TTL özelliği ve performanslı olması ile token bilgilerini Redis'de tutuluyor (Redis sevdiğim bir nosql çözümü). Role tabanlı erişim özelliği mevcut. Örnek olması adına bir admin kullıcısı mevcut, detaylarını aşağıda görüntüleyebilirsiniz.  


### Saga - Service Choreography

Service choreography is a global description of the participating services, which is defined by exchange of messages, rules of interaction and agreements between two or more endpoints. Choreography employs a decentralized approach for service composition.

![Alt text](./assets/saga-choreography.jpg?raw=true "Title")

### Kurulum

Proje assets klasörü içerindeki ```'docker-compose.yml'``` dosyasını kullanarak tüm bağımlılığı bulunan uygulamaları ve ek olarak servisleri ayağa kaldırarak localde derleme derdi olmadan test etmeye başlayabilirsiniz.

```
docker-compose -f docker-compose.yml up -d
```

```
https://hub.docker.com/repository/docker/umutaltas/altas-rig-ms/
```

### Servisler

| İsim | Swagger |
| ------ | ------ |
| Auth | [-> Swagger UI](localhost:8080/swagger-ui.html) |
| Customer | [-> Swagger UI](localhost:8081/swagger-ui.html) |
| Order | [-> Swagger UI](localhost:8082/swagger-ui.html) |
| Product | [-> Swagger UI](localhost:8083/swagger-ui.html) |



### Bağımlılıklar

- MongoDb (Mongo Express -> localhost:8089)
- Redis
- Kafka

### Postman 

Ana klasör altında yer alan assets klasöründe tüm servisler için oluşturulmuş Postman Collection'u import etip api testi yapabilirsiniz.

Servislerde ekleme ve silme işlemlerini sadece admin kullanıcı yapabiliyor. Collectionda ```'get Admin Token'``` ile hazır bir şekilde token edilebilirsiniz. Normal kullanıcılar için ```'register New User'``` ve sonrasında ```'get User Token'``` isteklerini çalıştırmanızı rica edeceğim.

### Eksik kalan bazı işler :)

1-CustomerId parametresi ile gidilen adresler için, kullanıcının başka bir kullanıcı customerId si ile işlem yapmasını engellemek adına bir kontrol gerekiyor. Kontrol çalışıyor fakat token'dan customerId bilgisini alamıyorum.  

``` 

@Override
    public void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .formLogin().disable()
                .anonymous().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET, "/api/customer/**").hasAnyRole("USER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/api/customer/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/api/customer/**").hasRole("USER")
                .antMatchers(HttpMethod.DELETE, "/api/customer/**").hasRole("ADMIN");
       -->      //.antMatchers("/api/customer/{id}").access("@securityWebExpression.checkCustomerId(principal, #id)");
    }
}
```


2- Uygulamaların çalışabileceği bir ortam şu an yok. Minishift ile halletmeyi düşündüm ama maalesef zamanım kalmadı.

3- Yina zaman kısıtı yüzünden test yazamadım.