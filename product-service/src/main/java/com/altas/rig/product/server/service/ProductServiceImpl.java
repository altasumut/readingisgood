package com.altas.rig.product.server.service;

import com.altas.rig.product.server.contstant.ProductConstants;
import com.altas.rig.product.server.enums.ActionType;
import com.altas.rig.product.server.model.Product;
import com.altas.rig.product.server.model.ProductCountEventDTO;
import com.altas.rig.product.server.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Objects;


@Service
@Transactional
public class ProductServiceImpl implements ProductService {

	@Autowired
	ReactiveMongoTemplate reactiveMongoTemplate;
	@Autowired
	private ProductRepository productRepository;

	@Override
	public Mono<Product> addproduct(Product product) {
		return productRepository.save(product);
	}

	@Override
	public Mono<Product> getProductById(String id) {
		return productRepository.findById(id);
	}

	@Override
	public Flux<Product> getAllProduct() {
		return productRepository.findAll().switchIfEmpty(Flux.empty());
	}

	@Override
	public Mono<Product> updateProduct(String productId, Product product) {
		Query updateQuery = new Query();
		updateQuery.addCriteria(Criteria.where(ProductConstants.PRODUCT_ID_KEY).is(productId));

		return reactiveMongoTemplate.findAndReplace(updateQuery, product, FindAndReplaceOptions.options().returnNew());
	}

	@Override
	public Mono<Product> deleteProduct(String productId) {
		final Mono<Product> product = getProductById(productId);
		if (Objects.isNull(product)) {
			return Mono.empty();
		}
		return getProductById(productId).switchIfEmpty(Mono.empty()).filter(Objects::nonNull).flatMap(productToBeDeleted -> productRepository
				.delete(productToBeDeleted).then(Mono.just(productToBeDeleted)));
	}


	public Product updateProductCount(ProductCountEventDTO productCountEventDTO) {
		Product product = reactiveMongoTemplate.findById(productCountEventDTO
						.getProductId(), Product.class)
				.block();

		Integer currentProductQuantity = product.getQtyInStock();

		if (ActionType.COUNT_INCREMENT.name().equals(productCountEventDTO.getActionType().name())) {
			product.setQtyInStock(currentProductQuantity + productCountEventDTO.getProductCount());
		} else {
			product.setQtyInStock(currentProductQuantity - productCountEventDTO.getProductCount());
		}

		Query updateQuery = new Query();
		updateQuery.addCriteria(Criteria.where(ProductConstants.PRODUCT_ID_KEY).is(product.getProducId()));
		return reactiveMongoTemplate.findAndReplace(updateQuery, product, FindAndReplaceOptions.options().returnNew()).block();

	}
}

	


