package com.altas.rig.product.server.service;


import com.altas.rig.product.server.model.Product;
import com.altas.rig.product.server.model.ProductCountEventDTO;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface ProductService {

    Mono<Product> addproduct(Product product);

    Mono<Product> getProductById(String id);

    Flux<Product> getAllProduct();

    Mono<Product> updateProduct(String productId, Product product);

    Mono<Product> deleteProduct(String productId);

    Product updateProductCount(ProductCountEventDTO productCountEventDTO);

}
