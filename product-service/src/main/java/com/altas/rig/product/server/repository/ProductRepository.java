package com.altas.rig.product.server.repository;

import com.altas.rig.product.server.model.Product;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;


public interface ProductRepository extends ReactiveMongoRepository<Product, String> {


}
