
package com.altas.rig.product.server.contstant;


public class ProductConstants {

    public static final String PRODUCT_ID_KEY = "_id";
    public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss.SSS";
    public static final String KAFKA_GROUP_ID = "productCountEvent";

    private ProductConstants() {
        throw new IllegalStateException("ProductConstants class");
    }

}
