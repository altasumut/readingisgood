package com.altas.rig.product.server.exception;

public class InternalServerError extends RuntimeException {
    public InternalServerError(String errorMessage) {
        super(errorMessage);
    }
}