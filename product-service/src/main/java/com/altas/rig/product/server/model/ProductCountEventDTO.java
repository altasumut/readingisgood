package com.altas.rig.product.server.model;


import com.altas.rig.product.server.enums.ActionType;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class ProductCountEventDTO {
    protected String orderId;
    protected String productId;
    protected int productCount;
    protected ActionType actionType;
}
