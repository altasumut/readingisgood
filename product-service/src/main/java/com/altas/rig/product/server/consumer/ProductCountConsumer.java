
package com.altas.rig.product.server.consumer;

import com.altas.rig.product.server.model.ProductCountEventDTO;
import com.altas.rig.product.server.service.ProductService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;


@Service

public class ProductCountConsumer {

    private static final Logger LOG = LoggerFactory.getLogger(ProductCountConsumer.class);

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ProductService productService;


    @KafkaListener(topics = "${kafka.message.productTopic.name}", containerFactory = "productCountLimitKafkaListenerContainerFactory")
    public void exchangeEventListener(String productCountlimit) {
        try {
            ProductCountEventDTO productCountEventDTO = objectMapper.readValue(productCountlimit, ProductCountEventDTO.class);
            productService.updateProductCount(productCountEventDTO);
        } catch (JsonProcessingException e) {
            LOG.error("Error Sending the Message and the exception is {}", e.getMessage());
        }
    }
}
