package com.altas.rig.product.server.handler;

import com.altas.rig.product.server.contstant.ProductConstants;
import com.altas.rig.product.server.enums.EventType;
import com.altas.rig.product.server.model.ExchangeEvent;
import com.altas.rig.product.server.producer.ExchangeEventProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import org.springframework.web.server.WebFilter;
import org.springframework.web.server.WebFilterChain;
import reactor.core.publisher.Mono;

import java.util.Objects;


@Component
@Slf4j
public class CustomHandlerInterceptor implements WebFilter {

    private static final Logger logger = LogManager.getLogger(CustomHandlerInterceptor.class);
    @Autowired
    ExchangeEventProducer exchangeEventProducer;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, WebFilterChain chain) {

        ExchangeEvent exchangeEvent = new ExchangeEvent();

        exchangeEvent.setTraceId(exchange.getRequest().getId());
        exchangeEvent.setEventType(EventType.ORDER_MS_REQUEST);
        exchangeEvent.setUri(exchange.getRequest().getURI().toString());
        exchangeEvent.setHttpMethodType(exchange.getRequest().getMethod());

        DateTime startedTime = DateTime.now();
        exchangeEvent.setRequestedAt(startedTime.toString(ProductConstants.DATE_FORMAT));

        ServerHttpResponse response = exchange.getResponse();

        return chain.filter(exchange)
                .doOnSuccess(done -> successResponseEventLog(response, exchangeEvent, startedTime))
                .doOnError(cause -> {
                    if (response.isCommitted()) {
                        errorResponseEventLog(exchangeEvent, response, cause, startedTime);
                    } else {
                        response.beforeCommit(() -> {
                            errorResponseEventLog(exchangeEvent, response, cause, startedTime);
                            return Mono.empty();
                        });
                    }
                });
    }

    private void successResponseEventLog(ServerHttpResponse response, ExchangeEvent exchangeEvent, DateTime startedTime) {
        try {
            DateTime completedTime = DateTime.now();
            exchangeEvent.setExecutionDuration(Seconds.secondsBetween(startedTime, completedTime).getSeconds() % 60 + " seconds");
            exchangeEvent.setExecutioncompletedAt(completedTime.toString(ProductConstants.DATE_FORMAT));
            exchangeEvent.setStatusCode(Objects.requireNonNull(response.getStatusCode()).value());
            exchangeEventProducer.sendExchangeEvent(exchangeEvent);
        } catch (JsonProcessingException e) {
            logger.error("Error Sending the Message and the exception is {}", e.getMessage());
        }
    }

    private void errorResponseEventLog(ExchangeEvent exchangeEvent, ServerHttpResponse response, Throwable cause, DateTime startedTime) {
        try {
            DateTime completedTime = DateTime.now();
            exchangeEvent.setExecutionDuration(Seconds.secondsBetween(startedTime, completedTime).getSeconds() % 60 + " seconds");
            exchangeEvent.setExecutioncompletedAt(completedTime.toString(ProductConstants.DATE_FORMAT));
            exchangeEvent.setStatusCode(Objects.requireNonNull(response.getStatusCode()).value());
            exchangeEvent.setErrorMsg(cause.getMessage());
            exchangeEventProducer.sendExchangeEvent(exchangeEvent);
        } catch (JsonProcessingException e) {
            logger.error("Error Sending the Message and the exception is {}", e.getMessage());
        }
    }
}
