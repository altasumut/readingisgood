package com.altas.rig.product.server.controller;


import com.altas.rig.product.server.model.Product;
import com.altas.rig.product.server.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/api/product")

public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping(value = "/addProduct")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Product> addProduct(@RequestBody Product product) {
        return productService.addproduct(product);

    }

    @GetMapping(value = "/getAllProduct")
    public Flux<Product> getAllProduct() {
        return productService.getAllProduct();
    }

    @GetMapping(value = "/getProduct/{id}")
    public Mono<ResponseEntity<Product>> getProductById(@PathVariable("id") final String id) {
        return productService.getProductById(id).map(product -> new ResponseEntity<>(product, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping(value = "/updateProduct/{id}")
    public Mono<Product> updateProduct(@PathVariable("id") final String id, @RequestBody Product product) {

        return productService.updateProduct(id, product);
    }

    @DeleteMapping("/deleteProduct/{id}")
    public Mono<Product> deleteProduct(@PathVariable final String id) {

        return productService.deleteProduct(id);
    }

    /*
     * @GetMapping("/publish/{event}") public String publishEvent(@PathVariable
     * String event) { System.out.println("inside publish event" + event); return
     * exchangeeventproducer.ExchangeEvent(event);
     *
     * }
     */
}