package com.altas.rig.product.server.enums;


public enum ActionType {
    COUNT_INCREMENT,
    COUNT_DECREMENT
}
