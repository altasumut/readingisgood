package com.altas.rig.customer.server.producer;

import com.altas.rig.customer.server.domain.dto.CustomerCreateEventDTO;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import java.math.BigInteger;
import java.util.UUID;

@Component
@Slf4j
public class CustomerCreateEventProducer {

    private static final Logger logger = LogManager.getLogger(CustomerCreateEventProducer.class);
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    private KafkaTemplate<String, String> customKafkaTemplate;
    @Value(value = "${kafka.message.customerCreateTopic.name}")
    private String topicName;

    public ListenableFuture<SendResult<String, String>> sendCustomerCreateEvent(CustomerCreateEventDTO customerCreateEvent) throws JsonProcessingException {

        String message = objectMapper.writeValueAsString(customerCreateEvent);
        objectMapper.setSerializationInclusion(Include.NON_NULL);

        String lUUID = String.format("%040d", new BigInteger(UUID.randomUUID().toString().replace("-", ""), 16));

        ListenableFuture<SendResult<String, String>> listenableFuture = customKafkaTemplate.send(topicName, lUUID, message);

        listenableFuture.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
            @Override
            public void onFailure(Throwable ex) {
                handleFailure(ex);
            }

            @Override
            public void onSuccess(SendResult<String, String> result) {
                handleSuccess(message, result);
            }
        });

        return listenableFuture;
    }

    private void handleFailure(Throwable ex) {
        logger.error("Error Sending the Message and the exception is {}", ex.getMessage());
    }


    private void handleSuccess(String value, SendResult<String, String> result) {
        logger.info("Message Sent SuccessFully for the value is {} , partition is {}", value, result.getRecordMetadata().partition());
    }
}
