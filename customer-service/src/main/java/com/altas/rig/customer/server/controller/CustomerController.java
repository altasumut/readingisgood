package com.altas.rig.customer.server.controller;

import com.altas.rig.customer.server.domain.entity.Address;
import com.altas.rig.customer.server.domain.entity.Customer;
import com.altas.rig.customer.server.service.CustomerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.Date;

@RestController
@RequestMapping("/api/customer")
public class CustomerController {

    private static final Logger LOG = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    CustomerService customerService;


    @GetMapping(value = "/")
    public Flux<Customer> getAllCustomer() {
        LOG.debug("Get all Customers call initiated at : [{}]", new Date());
        return customerService.findAllCustomer();
    }

    @GetMapping(value = "/me")
    public Mono<ResponseEntity<Customer>> getCurrentCustomer(Principal principal) {
        LOG.debug("Get current Customer call initiated at : [{}]", new Date());
        return customerService.findCurrentCustomer(principal.getName())
                .map(customer -> new ResponseEntity<>(customer, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = "/")
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Customer> createItem(@RequestBody Customer customer) {
        LOG.debug("Create Customer call initiated at [" + new Date() + "] with request payload: {}", customer.toString());
        return customerService.saveCustomer(customer);
    }

    @GetMapping(value = "/{id}")
    public Mono<ResponseEntity<Customer>> getCustomerById(@PathVariable String id) {
        LOG.debug("Get Customer by Customer Id[{}] call initiated at : [{}]", id, new Date());
        return customerService.findCustomerById(id)
                .map(customer -> new ResponseEntity<>(customer, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping(value = "/{id}")
    public Mono<ResponseEntity<Customer>> updateItem(@PathVariable String id, @RequestBody Customer customer) {
        LOG.debug("Update Customer[Customer Id: {}] call initiated at [{}] with request payload: {}", id, new Date(), customer);
        return customerService.updateCustomer(id, customer)
                .map(customerRes -> new ResponseEntity<>(customerRes, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PutMapping(value = "/{id}/address")
    public Mono<ResponseEntity<Customer>> updateAddress(@PathVariable String id, @RequestBody Address address) {
        LOG.debug("Update Address Customer[Customer Id: {}] call initiated at [{}] with request payload: {}", id, new Date(), address);
        return customerService.updateAddress(id, address)
                .map(customerRes -> new ResponseEntity<>(customerRes, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping(value = "/{id}")
    public Mono<ResponseEntity<Customer>> deleteItem(@PathVariable String id) {
        LOG.debug("Delete Customer by Customer Id[{}] call initiated at : [{}}]", id, new Date());
        return customerService.deleteCustomer(id)
                .map(customer -> new ResponseEntity<>(customer, HttpStatus.OK))
                .defaultIfEmpty(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }
}