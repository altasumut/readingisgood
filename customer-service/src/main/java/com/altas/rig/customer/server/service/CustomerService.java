package com.altas.rig.customer.server.service;

import com.altas.rig.customer.server.domain.dto.CustomerCreditLimitEventDTO;
import com.altas.rig.customer.server.domain.entity.Address;
import com.altas.rig.customer.server.domain.entity.Customer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CustomerService {

	Mono<Customer> saveCustomer(Customer customer);

	Mono<Customer> findCustomerById(String customerId);

	Mono<Customer> findCurrentCustomer(String username);

	Flux<Customer> findAllCustomer();

	Mono<Customer> updateCustomer(String customerId, Customer customer);

	Mono<Customer> updateAddress(String customerId, Address address);

	Mono<Customer> deleteCustomer(String customerId);

	void updateCustomerCreditLimit(CustomerCreditLimitEventDTO customerCreditLimitEvent);
}
