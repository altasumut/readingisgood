package com.altas.rig.customer.server.enums;

public enum EventType {
    CUSTOMER_MS_REQUEST,
    ORDER_MS_REQUEST,
    PRODUCT_MS_REQUEST
}
