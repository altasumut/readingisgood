package com.altas.rig.customer.server.constant;

public class Constants {

    public static final String CUSTOMER_ID_KEY = "id";
    public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss.SSS";
    public static final String KAFKA_GROUP_ID = "customerCreditEvent";

    private Constants() {
        throw new IllegalStateException("Constants class");
    }
}
