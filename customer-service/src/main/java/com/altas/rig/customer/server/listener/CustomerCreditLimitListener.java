package com.altas.rig.customer.server.listener;

import com.altas.rig.customer.server.domain.dto.CustomerCreditLimitEventDTO;
import com.altas.rig.customer.server.service.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.concurrent.CountDownLatch;

@Component
@Slf4j
public class CustomerCreditLimitListener {

    private static final Logger logger = LogManager.getLogger(CustomerCreditLimitListener.class);
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    CustomerService customerService;
    private CountDownLatch customerCreditLimitLatch = new CountDownLatch(1);

    @KafkaListener(topics = "${kafka.message.customerCreditTopic.name}", containerFactory = "customerCreditLimitKafkaListenerContainerFactory")
    public void exchangeEventListener(String customerCreditLimitStr) {
        try {
            CustomerCreditLimitEventDTO customerCreditLimitEvent = objectMapper.readValue(customerCreditLimitStr, CustomerCreditLimitEventDTO.class);
            logger.info("Recieved message: {}", customerCreditLimitEvent);
            customerService.updateCustomerCreditLimit(customerCreditLimitEvent);
            this.customerCreditLimitLatch.countDown();
        } catch (JsonProcessingException e) {
            logger.error("Error Sending the Message and the exception is {}", e.getMessage());
        }
    }
}
