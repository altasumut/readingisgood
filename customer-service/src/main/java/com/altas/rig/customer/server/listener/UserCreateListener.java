package com.altas.rig.customer.server.listener;

import com.altas.rig.customer.server.domain.dto.CustomerCreateEventDTO;
import com.altas.rig.customer.server.domain.dto.UserCreateEventDTO;
import com.altas.rig.customer.server.domain.entity.Customer;
import com.altas.rig.customer.server.producer.CustomerCreateEventProducer;
import com.altas.rig.customer.server.service.CustomerService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@Slf4j
public class UserCreateListener {

    private static final Logger logger = LogManager.getLogger(UserCreateListener.class);
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    CustomerService customerService;
    @Autowired
    CustomerCreateEventProducer producer;

    @KafkaListener(topics = "${kafka.message.userTopic.name}", containerFactory = "userCreateKafkaListenerContainerFactory")
    public void userCreateEventListener(String userCreateStr) {
        try {
            UserCreateEventDTO createEvent = objectMapper.readValue(userCreateStr, UserCreateEventDTO.class);
            logger.info("Recieved message: {}", createEvent);
            customerService.saveCustomer(Customer.builder()
                    .firstName(createEvent.getFirstName())
                    .lastName(createEvent.getLastName())
                    .phoneNumber(createEvent.getPhoneNumber())
                    .email(createEvent.getEmail())
                    .createdDate(new Date())
                    .version(1L)
                    .creditLimit(0.0)
                    .build()).subscribe(this::initiateKafkaEventForCustCreate);
        } catch (JsonProcessingException e) {
            logger.error("Error Sending the Message and the exception is {}", e.getMessage());
        }
    }

    private void initiateKafkaEventForCustCreate(Customer customer) {
        try {
            producer.sendCustomerCreateEvent(new CustomerCreateEventDTO(customer.id, customer.email));
        } catch (Exception e) {
            throwRuntimeException(e);
        }
    }

    private void throwRuntimeException(Exception e) {
        throw new RuntimeException(e);
    }

}
