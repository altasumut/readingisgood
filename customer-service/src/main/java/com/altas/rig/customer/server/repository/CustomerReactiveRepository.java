package com.altas.rig.customer.server.repository;

import com.altas.rig.customer.server.domain.entity.Customer;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CustomerReactiveRepository extends ReactiveMongoRepository<Customer, String> {

    Mono<Customer> findByEmail(final String email);
}
