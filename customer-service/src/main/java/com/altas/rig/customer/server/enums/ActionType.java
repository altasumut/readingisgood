package com.altas.rig.customer.server.enums;

public enum ActionType {
    CREDIT_INCREMENT,
    CREDIT_DECREMENT
}
