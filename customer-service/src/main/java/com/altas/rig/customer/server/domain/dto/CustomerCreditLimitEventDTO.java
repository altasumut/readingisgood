package com.altas.rig.customer.server.domain.dto;

import com.altas.rig.customer.server.enums.ActionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class CustomerCreditLimitEventDTO {

    private String orderId;
    private String customerId;
    private ActionType actionType;
    private Double creditLimitAmount;


}
