package com.altas.rig.customer.server.service.impl;

import com.altas.rig.customer.server.constant.Constants;
import com.altas.rig.customer.server.domain.dto.CustomerCreditLimitEventDTO;
import com.altas.rig.customer.server.domain.entity.Address;
import com.altas.rig.customer.server.domain.entity.Customer;
import com.altas.rig.customer.server.enums.ActionType;
import com.altas.rig.customer.server.repository.CustomerReactiveRepository;
import com.altas.rig.customer.server.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndReplaceOptions;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    ReactiveMongoTemplate reactiveMongoTemplate;
    @Autowired
    private CustomerReactiveRepository customerReactiveRepository;

    @Override
    public Mono<Customer> saveCustomer(Customer customer) {
        return customerReactiveRepository.insert(customer);
    }

    @Override
    public Mono<Customer> findCustomerById(String customerId) {
        return customerReactiveRepository.findById(customerId);
    }

    @Override
    public Mono<Customer> findCurrentCustomer(String username) {
        return customerReactiveRepository.findByEmail(username);
    }

    @Override
    public Flux<Customer> findAllCustomer() {
        return customerReactiveRepository.findAll();
    }

    @Override
    public Mono<Customer> updateCustomer(String customerId, Customer customer) {
        Query updateQuery = new Query();
        updateQuery.addCriteria(Criteria.where(Constants.CUSTOMER_ID_KEY).is(customerId));
        return reactiveMongoTemplate.findAndReplace(updateQuery, customer, FindAndReplaceOptions.options().returnNew());
    }

    @Override
    public Mono<Customer> updateAddress(String customerId, Address address) {
        Customer customer = customerReactiveRepository.findById(customerId).block();
        customer.setAddress(address);

        Query updateQuery = new Query();
        updateQuery.addCriteria(Criteria.where(Constants.CUSTOMER_ID_KEY).is(customerId));
        return reactiveMongoTemplate.findAndReplace(updateQuery, customer, FindAndReplaceOptions.options().returnNew());
    }

    @Override
    public Mono<Customer> deleteCustomer(String customerId) {
        Query deleteQuery = new Query();
        deleteQuery.addCriteria(Criteria.where(Constants.CUSTOMER_ID_KEY).is(customerId));
        return reactiveMongoTemplate.findAndRemove(deleteQuery, Customer.class);
    }

    @Override
    public void updateCustomerCreditLimit(CustomerCreditLimitEventDTO customerCreditLimitEvent) {
        reactiveMongoTemplate.findById(customerCreditLimitEvent.getCustomerId(), Customer.class)
                .subscribe(customer -> {
                    Double currentCreditLimit = customer.getCreditLimit();

                    if (ActionType.CREDIT_INCREMENT.name().equals(
                            customerCreditLimitEvent.getActionType().name())) {
                        customer.setCreditLimit(currentCreditLimit + customerCreditLimitEvent.getCreditLimitAmount());
                    } else {
                        customer.setCreditLimit(currentCreditLimit - customerCreditLimitEvent.getCreditLimitAmount());
                    }

                    Query updateQuery = new Query();
                    updateQuery.addCriteria(Criteria.where(Constants.CUSTOMER_ID_KEY).is(customer.getId()));
                    reactiveMongoTemplate
                            .findAndReplace(updateQuery, customer, FindAndReplaceOptions.options().returnNew()).block();
                });
    }
}