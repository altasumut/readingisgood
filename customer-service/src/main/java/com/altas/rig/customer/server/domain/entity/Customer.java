package com.altas.rig.customer.server.domain.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.*;
import org.springframework.data.annotation.*;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Document("customers")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@JsonPropertyOrder({
        "id",
        "firstName",
        "lastName",
        "phoneNumber",
        "email",
        "address",
        "creditLimit",
        "version",
        "createdBy",
        "createdDate",
        "updatedBy",
        "updatedDate"
})
public class Customer implements Serializable {

    private static final long serialVersionUID = 6938865787322676118L;

    @Id
    @JsonProperty("id")
    public String id;
    @JsonProperty("firstName")
    public String firstName;
    @JsonProperty("lastName")
    public String lastName;
    @JsonProperty("phoneNumber")
    public String phoneNumber;
    @JsonProperty("email")
    public String email;
    @JsonProperty("address")
    public Address address;
    @JsonProperty("creditLimit")
    public Double creditLimit;
    @Version
    @JsonProperty("version")
    public long version = 1L;
    @CreatedBy
    @JsonProperty("createdBy")
    public String createdBy;
    @CreatedDate
    @JsonProperty("createdDate")
    public Date createdDate;
    @LastModifiedBy
    @JsonProperty("updatedBy")
    public String updatedBy;
    @LastModifiedDate
    @JsonProperty("updatedDate")
    public Date updatedDate;
}
