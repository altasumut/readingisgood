package com.altas.rig.auth.server.repository;

import com.altas.rig.auth.server.entity.Role;
import com.altas.rig.auth.server.model.ERole;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface RoleRepository extends ReactiveMongoRepository<Role, String> {
    Mono<Role> findByRoleId(ERole name);
}
