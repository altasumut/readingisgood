package com.altas.rig.auth.server.listener;

import com.altas.rig.auth.server.model.dto.CustomerCreateEventDTO;
import com.altas.rig.auth.server.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustomerCreateListener {

    private static final Logger logger = LogManager.getLogger(CustomerCreateListener.class);
    @Autowired
    ObjectMapper objectMapper;
    @Autowired
    UserService userService;

    @KafkaListener(topics = "${kafka.message.customerCreateTopic.name}", containerFactory = "customerCreateKafkaListenerContainerFactory")
    public void userCreateEventListener(String custCreateStr) {
        try {
            logger.info("Recieved message: {}", custCreateStr);
            CustomerCreateEventDTO createEvent = objectMapper.readValue(custCreateStr, CustomerCreateEventDTO.class);
            userService.setCustomerId(createEvent);
        } catch (JsonProcessingException e) {
            logger.error("Error Sending the Message and the exception is {}", e.getMessage());
        }
    }
}
