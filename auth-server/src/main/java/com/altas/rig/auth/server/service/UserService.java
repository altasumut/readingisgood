package com.altas.rig.auth.server.service;

import com.altas.rig.auth.server.entity.Role;
import com.altas.rig.auth.server.entity.User;
import com.altas.rig.auth.server.exception.InternalServerError;
import com.altas.rig.auth.server.model.ERole;
import com.altas.rig.auth.server.model.MessageResponse;
import com.altas.rig.auth.server.model.SignupRequest;
import com.altas.rig.auth.server.model.dto.CustomerCreateEventDTO;
import com.altas.rig.auth.server.model.dto.UserCreateEventDTO;
import com.altas.rig.auth.server.producer.UserCreateEventProducer;
import com.altas.rig.auth.server.repository.RoleRepository;
import com.altas.rig.auth.server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    ReactiveMongoTemplate reactiveMongoTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    UserDetailsServiceImpl userDetailsServiceImpl;

    @Autowired
    UserCreateEventProducer producer;

    public ResponseEntity<MessageResponse> createUser(SignupRequest signUpRequest) {
        if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getEmail()).block())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        User user = new User(signUpRequest.getEmail(), encoder.encode(signUpRequest.getPassword()));
        user.setRoles(getDefaultRole());
        userRepository.save(user).subscribe(newUser -> initiateKafkaEventForCreate(newUser.getUsername(), signUpRequest));
        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    public void setCustomerId(CustomerCreateEventDTO customerCreateEvent) {
        userRepository.findByUsername(customerCreateEvent.getUsername())
                .switchIfEmpty(Mono.error(new Exception("USER_NOT_FOUND")))
                .map(user -> {
                    user.setCustomerId(customerCreateEvent.getId());
                    return user;
                })
                .flatMap(userRepository::save).block();
    }

    private Set<Role> getDefaultRole() {
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.findByRoleId(ERole.ROLE_USER).block());
        return roles;
    }

    private void initiateKafkaEventForCreate(String username, SignupRequest signUpRequest) {
        UserCreateEventDTO createEventDTO = new UserCreateEventDTO(username, signUpRequest.getFirstName(), signUpRequest.getLastName(), signUpRequest.getPhoneNumber(), signUpRequest.getEmail());
        try {
            producer.sendUserCreateEvent(createEventDTO);
        } catch (Exception e) {
            throwRuntimeException(e);
        }
    }

    private void throwRuntimeException(Exception e) {
        throw new InternalServerError(e.getMessage());
    }
}