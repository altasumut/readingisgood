package com.altas.rig.auth.server.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
public class SignupRequest {

    @NotBlank
    @Size(max = 50)
    @Email
    private String email;

    @NotBlank
    @Size(max = 20)
    @Transient
    private String firstName;

    @NotBlank
    @Size(max = 20)
    @Transient
    private String lastName;

    @NotBlank
    @Size(min = 10, max = 10, message = "please enter valid phone number -> 532XXXXXXX ")
    @Transient
    private String phoneNumber;

    @NotBlank
    @Size(min = 6, max = 40, message = "please enter valid password min:6, max:40")
    private String password;
}
