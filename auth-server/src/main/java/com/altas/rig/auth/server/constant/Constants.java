package com.altas.rig.auth.server.constant;

public class Constants {

    public static final String USER_ID_KEY = "id";
    public static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss.SSS";
    public static final String KAFKA_GROUP_ID = "userCreateEvent";

    private Constants() {
        throw new IllegalStateException("Constants class");
    }
}
