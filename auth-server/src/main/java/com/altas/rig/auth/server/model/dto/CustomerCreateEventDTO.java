package com.altas.rig.auth.server.model.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerCreateEventDTO {
    private String id;
    private String username;
}
