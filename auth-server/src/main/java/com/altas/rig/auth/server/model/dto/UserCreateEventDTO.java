package com.altas.rig.auth.server.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserCreateEventDTO {
    private String username;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
}
