package com.altas.rig.auth.server.entity;

import com.altas.rig.auth.server.model.ERole;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Document(collection = "roles")
public class Role {
    @Id
    private String id;
    private ERole roleId;
    private String roleName;
}
