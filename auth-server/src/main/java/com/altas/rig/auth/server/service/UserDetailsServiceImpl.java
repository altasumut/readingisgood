package com.altas.rig.auth.server.service;

import com.altas.rig.auth.server.entity.User;
import com.altas.rig.auth.server.repository.RoleRepository;
import com.altas.rig.auth.server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import reactor.core.publisher.Mono;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = Mono.just(username)
                .flatMap(userRepository::findByUsername)
                .switchIfEmpty(Mono.error(new UsernameNotFoundException("User Not Found with username: " + username))).block();

        return UserDetailsImpl.build(user);
    }
}
