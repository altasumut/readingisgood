package com.altas.rig.auth.server.exception;

public class InternalServerError extends RuntimeException {
    public InternalServerError(String errorMessage) {
        super(errorMessage);
    }
}