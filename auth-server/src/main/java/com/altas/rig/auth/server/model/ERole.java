package com.altas.rig.auth.server.model;

public enum ERole {
    ROLE_USER,
    ROLE_OTHER,
    ROLE_ADMIN
}
