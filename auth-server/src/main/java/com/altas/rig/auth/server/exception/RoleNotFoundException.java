package com.altas.rig.auth.server.exception;

public class RoleNotFoundException extends InternalServerError {
    public RoleNotFoundException() {
        super("User Role not found!");
    }
}
